"use strict";
jQuery(document).ready(function($) {
    // Window on load
    $(window).on('load', function() {
        if (Services.is('.banner__slider')) {
            $('.banner__slider').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                dots: false,
                items: 1
            });
        }
        if ($('.project-slide').length != 0) {
            $('.project-slide .owl-carousel').owlCarousel({
                loop: true,
                nav: true,
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 2
                    }
                }
            });
        }
    });
});